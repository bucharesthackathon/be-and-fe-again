FROM openjdk:21

WORKDIR /app

COPY build/libs/profilesApi-0.0.1-SNAPSHOT.jar .

CMD ["java", "-jar", "profilesApi-0.0.1-SNAPSHOT.jar"]
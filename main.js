// private Set<String> extractUniqueAddresses(String addresses) {
//     addresses = addresses.strip().replace("0x", "");
//     Pattern pattern = Pattern.compile("");
//     Matcher matcher = pattern.matcher(addresses);
//     Set<String> result = new TreeSet<>();
//     while (matcher.find()) {
//         result.add(matcher.group(1));
//     }
//     return result.stream()
//         .map(e -> "0".repeat(Math.max(0, 40 - e.length())) + e)
//         .collect(Collectors.toSet());
// }

function removeZeros(addresses) {
    addresses = addresses.trim().replace("0x", "");
    const matches = addresses.matchAll(/0+([1-9a-f]+)/g);

    if (!matches) return new Set();

    const result = new Set([...matches].map(m => "".repeat(Math.max(0, 40 - m[1].length)) + m[1]))

    return result;
}

const addresses = "0x0000000000000000000000000ccb12a69fa9fea6c1dc3af1c8498cb2c8b4bb8c0000000000000000000000000ccb12a69fa9fea6c1dc3af1c8498cb2c8b4bb8c00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
console.log(removeZeros(addresses))
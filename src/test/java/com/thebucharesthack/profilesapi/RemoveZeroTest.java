package com.thebucharesthack.profilesapi;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class RemoveZeroTest {

    @Test
    public void test() {
        String addresses = """
                0x0000000000000000000000000ccb12a69fa9fea6c1dc3af1c8498cb2c8b4bb8c0000000000000000000000000ccb12a69fa9fea6c1dc3af1c8498cb2c8b4bb8c00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
                """.strip().replace("0x", "");

        Set<String> actual = extractUniqueAddresses(addresses);
        Set<String> expected = new TreeSet<>(List.of("0ccb12a69fa9fea6c1dc3af1c8498cb2c8b4bb8c", "0ccb12a69fa9fea6c1dc3af1c8498cb2c8b4bb8c"));

        Assertions.assertEquals(expected, actual);

    }

    private Set<String> extractUniqueAddresses(String addresses) {
        addresses = addresses.strip().replace("0x", "");
        Pattern pattern = Pattern.compile("0+([1-9a-f]+)");
        Matcher matcher = pattern.matcher(addresses);
        Set<String> result = new TreeSet<>();
        while (matcher.find()) {
            result.add(matcher.group(1));
        }
        return result.stream()
                .map(e -> "0".repeat(Math.max(0, 40 - e.length())) + e)
                .collect(Collectors.toSet());
    }

}

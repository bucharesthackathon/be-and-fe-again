package com.thebucharesthack.profilesapi.ai;

import com.thebucharesthack.profilesapi.dto.EmbeddingsResponse;
import jakarta.annotation.PostConstruct;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.Objects;

@Component
public class OpenAiEmbeddings {

    private static final String API_URI = "https://api.openai.com/v1/embeddings";

    @Value("${openai.embeddings.key}")
    private String apiKey;
    @Value("${openai.embeddings.model:}")
    private String model;

    @PostConstruct
    void validate() {
        if (StringUtils.isBlank(apiKey)) {
            throw new RuntimeException("The model for OpenAI Embeddings should be provided via the property openai.embeddings.key");
        }
        if (StringUtils.isBlank(model)) {
            throw new RuntimeException("The model for OpenAI Embeddings should be provided via the property openai.embeddings.model");
        }
    }

    public EmbeddingsResponse getEmbeddingFor(String content) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        headers.set("Authorization", "Bearer " + apiKey);
        HttpEntity<Object> entity = new HttpEntity<>(Map.of("input", content, "model", model), headers);
        return restTemplate.postForObject(API_URI, entity, EmbeddingsResponse.class);
    }

}

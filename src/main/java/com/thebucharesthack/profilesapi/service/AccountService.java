package com.thebucharesthack.profilesapi.service;

import com.thebucharesthack.profilesapi.dto.AccountDetailsResponse;
import com.thebucharesthack.profilesapi.dto.request.AccountWalletUpdateRequest;
import com.thebucharesthack.profilesapi.entity.AccountEntity;
import com.thebucharesthack.profilesapi.mapper.AccountMapper;
import com.thebucharesthack.profilesapi.repository.AccountRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AccountService {

    private AccountMapper mapper;
    private AccountRepository accountRepository;

    public AccountDetailsResponse findDetailsByLinkedInUri(String linkedInUri) {
        return accountRepository.findByLinkedInProfileUri(linkedInUri)
                .map(mapper::entityToResponse)
                .orElseThrow(() -> new RuntimeException("No user found by linkedIn uri: " + linkedInUri));
    }

    public void updateWallet(AccountWalletUpdateRequest walletUpdateRequest) {
        AccountEntity entity = accountRepository.findByLinkedInProfileUri(walletUpdateRequest.getLinkedInUri())
                .orElseThrow(() -> new RuntimeException("No user found by linkedIn uri: " + walletUpdateRequest.getLinkedInUri()));
        entity.setWalletPrivateKey(walletUpdateRequest.getPrivateKey());
        entity.setWalletAddress(walletUpdateRequest.getAddress());
        accountRepository.save(entity);
    }

}

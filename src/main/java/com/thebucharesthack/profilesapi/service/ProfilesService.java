package com.thebucharesthack.profilesapi.service;

import com.thebucharesthack.profilesapi.ai.OpenAiEmbeddings;
import com.thebucharesthack.profilesapi.dto.ClosestProfileResponse;
import com.thebucharesthack.profilesapi.dto.EmbeddingsResponse;
import com.thebucharesthack.profilesapi.dto.EmbeddingsResponseData;
import com.thebucharesthack.profilesapi.repository.ProfileRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ProfilesService {

    private ProfileRepository profileRepository;
    private OpenAiEmbeddings openAiEmbeddings;

    public List<ClosestProfileResponse> getClosestProfilesByDescription(String jobDescription, Integer limit) {
        String embeddings = Optional.ofNullable(openAiEmbeddings.getEmbeddingFor(jobDescription))
                .map(EmbeddingsResponse::getData)
                .map(List::getFirst)
                .map(EmbeddingsResponseData::getEmbedding)
                .map(Object::toString)
                .orElseThrow(() -> new RuntimeException("Couldn't get embeddings for the given job description"));
        return profileRepository.getClosestProfilesByDescription(embeddings, limit);
    }

    public List<ClosestProfileResponse> getClosestProfilesByProfile(String linkedInProfileUrl, Integer limit) {
        String embeddings = profileRepository.getSqlLinkedinEmbeddingByLink(linkedInProfileUrl);
        return profileRepository.getClosestProfilesByProfile(embeddings, limit);
    }

}

package com.thebucharesthack.profilesapi.rowMappers;

import com.thebucharesthack.profilesapi.dto.ClosestProfileResponse;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class ClosestProfileRowMapper implements RowMapper<ClosestProfileResponse> {

    @Override
    public ClosestProfileResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
        return ClosestProfileResponse.builder()
                .id(rs.getInt(1))
                .firstName(rs.getString(2))
                .lastName(rs.getString(3))
                .fullName(rs.getString(4))
                .profileImageUrl(rs.getString(5))
                .linkedInPublicIdentifier(rs.getString(6))
                .linkedInProfileUrl(rs.getString(7))
                .headline(rs.getString(8))
                .countryFullName(rs.getString(9))
                .countryCode(rs.getString(10))
                .state(rs.getString(11))
                .city(rs.getString(12))
                .description(rs.getString(13))
                .build();
    }

}

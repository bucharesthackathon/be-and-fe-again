package com.thebucharesthack.profilesapi.entity;

import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "account")
public class AccountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "wallet_private_key")
    private String walletPrivateKey;
    @Column(name = "wallet_address")
    private String walletAddress;
    @Column(name = "linkedin_profile_uri")
    private String linkedInProfileUri;
    @Column(name = "title")
    private String title;
    @Column(name = "activity_domain")
    private String domain;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "profile_image")
    private String profileImage;

}

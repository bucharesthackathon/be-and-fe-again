package com.thebucharesthack.profilesapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "embeddings", schema = "public")
public class EmbeddingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer id;

    @Column(name = "description", nullable = false)
    private String description;
    @Column(name = "description_embedding", nullable = false)
    private String descriptionEmbedding;
    @Column(name = "full_linkedin_profile", nullable = false)
    private String fullLinkedInProfile;
    @Column(name = "clean_linkedin_profile", nullable = false)
    private String cleanLinkedInProfile;
    @Column(name = "clean_linkedin_profile_embedding", nullable = false)
    private String cleanLinkedInProfileEmbedding;

}

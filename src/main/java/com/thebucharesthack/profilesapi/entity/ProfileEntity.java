package com.thebucharesthack.profilesapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "profiles", schema = "public")
public class ProfileEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer id;

    @Column(name = "first_name", nullable = false)
    private String firstName;
    @Column(name = "last_name", nullable = false)
    private String lastName;
    @Column(name = "full_name", nullable = false)
    private String fullName;
    @Column(name = "profile_pic_url")
    private String profilePictureUrl;
    @Column(name = "linkedin_public_indentifier", nullable = false)
    private String linkedInPublicIdentifier;
    @Column(name = "linkedin_profile_url", nullable = false)
    private String linkedInProfileUrl;
    @Column(name = "headline")
    private String headline;
    @Column(name = "countryFullName")
    private String countryFullName;
    @Column(name = "country_code")
    private String countryCode;
    @Column(name = "state")
    private String state;
    @Column(name = "city")
    private String city;

}

package com.thebucharesthack.profilesapi;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.NONE)
public final class Mappings {

    public static final String API = "/api";

    public static final String PROFILES = API + "/profiles";
    public static final String ACCOUNT = API + "/account";

}

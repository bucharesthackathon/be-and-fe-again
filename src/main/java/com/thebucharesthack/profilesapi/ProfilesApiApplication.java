package com.thebucharesthack.profilesapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProfilesApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProfilesApiApplication.class, args);
    }

}

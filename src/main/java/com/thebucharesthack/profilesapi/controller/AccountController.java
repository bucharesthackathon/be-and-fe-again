package com.thebucharesthack.profilesapi.controller;

import com.thebucharesthack.profilesapi.Mappings;
import com.thebucharesthack.profilesapi.dto.AccountDetailsResponse;
import com.thebucharesthack.profilesapi.dto.request.AccountWalletUpdateRequest;
import com.thebucharesthack.profilesapi.service.AccountService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping(Mappings.ACCOUNT)
@CrossOrigin(originPatterns = "*", maxAge = 3600)
public class AccountController {

    private AccountService accountService;

    @GetMapping
    public AccountDetailsResponse getAccountByLinkedInUri(@RequestParam(name = "url", required = true) String url) {
        return accountService.findDetailsByLinkedInUri(url);
    }

    @PostMapping("/wallet")
    public void updateWallet(@RequestBody AccountWalletUpdateRequest body) {
        accountService.updateWallet(body);
    }

}

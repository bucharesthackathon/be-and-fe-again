package com.thebucharesthack.profilesapi.controller;

import com.thebucharesthack.profilesapi.Mappings;
import com.thebucharesthack.profilesapi.dto.ClosestProfileResponse;
import com.thebucharesthack.profilesapi.dto.request.SearchJobDescriptionRequest;
import com.thebucharesthack.profilesapi.dto.request.SearchSimilarProfileRequest;
import com.thebucharesthack.profilesapi.service.ProfilesService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping(Mappings.PROFILES)
@CrossOrigin(originPatterns = "*", maxAge = 3600)
public class ProfilesController {

    private ProfilesService profilesService;

    @PostMapping("/closest/description")
    public List<ClosestProfileResponse> getClosestProfilesByDescription(
            @RequestParam(required = false, defaultValue = "5") Integer limit,
            @RequestBody SearchJobDescriptionRequest jobDescriptionRequest
    ) {
        return profilesService.getClosestProfilesByDescription(jobDescriptionRequest.getJobDescription(), limit);
    }

    @PostMapping("/closest/profile")
    public List<ClosestProfileResponse> getClosestProfilesByProfile(
            @RequestParam(required = false, defaultValue = "5") Integer limit,
            @RequestBody SearchSimilarProfileRequest similarProfileRequest
            ) {
        return profilesService.getClosestProfilesByProfile(similarProfileRequest.getLinkedInUri(), limit);
    }

}

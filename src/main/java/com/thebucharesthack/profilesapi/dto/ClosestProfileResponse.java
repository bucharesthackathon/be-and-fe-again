package com.thebucharesthack.profilesapi.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClosestProfileResponse {

    public Integer id;
    public String firstName;
    public String lastName;
    public String fullName;
    public String profileImageUrl;
    public String linkedInPublicIdentifier;
    public String linkedInProfileUrl;
    public String headline;
    public String countryFullName;
    public String countryCode;
    public String state;
    public String city;
    public String description;

}

package com.thebucharesthack.profilesapi.dto;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AccountDetailsResponse {

    private Integer id;
    private String walletPrivateKey;
    private String walletAddress;
    private String linkedInProfileUri;
    private String title;
    private String domain;
    private String firstName;
    private String lastName;
    private String profileImage;

}

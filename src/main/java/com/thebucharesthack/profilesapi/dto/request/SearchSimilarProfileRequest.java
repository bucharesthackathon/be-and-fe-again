package com.thebucharesthack.profilesapi.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SearchSimilarProfileRequest {

    private String linkedInUri;

}

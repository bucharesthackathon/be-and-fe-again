package com.thebucharesthack.profilesapi.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AccountWalletUpdateRequest {

    private String linkedInUri;
    private String privateKey;
    private String address;

}

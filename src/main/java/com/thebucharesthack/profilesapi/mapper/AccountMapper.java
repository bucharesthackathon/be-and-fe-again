package com.thebucharesthack.profilesapi.mapper;

import com.thebucharesthack.profilesapi.dto.AccountDetailsResponse;
import com.thebucharesthack.profilesapi.entity.AccountEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AccountMapper {

    AccountDetailsResponse entityToResponse(AccountEntity entity);

}

package com.thebucharesthack.profilesapi.repository;

import com.thebucharesthack.profilesapi.entity.AccountEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AccountRepository extends CrudRepository<AccountEntity, Integer> {

    Optional<AccountEntity> findByLinkedInProfileUri(String linkedInProfileUri);

}

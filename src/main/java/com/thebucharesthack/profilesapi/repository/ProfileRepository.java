package com.thebucharesthack.profilesapi.repository;

import com.thebucharesthack.profilesapi.dto.ClosestProfileResponse;
import com.thebucharesthack.profilesapi.rowMappers.ClosestProfileRowMapper;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
@AllArgsConstructor
public class ProfileRepository {

    private static final String SQL_CLOSEST_BY_DESCRIPTION = """
                SELECT p.id, p.first_name, p.last_name, p.full_name, 
                    p.profile_pic_url as profile_image_url, 
                    p.linkedin_public_identifier, p.linkedin_profile_url, 
                    p.headline, p.country_full_name, p.country_code, p.state, 
                    p.city, closest_5.description
                FROM (
                    SELECT user_id, description, embeddings.description_embedding <-> ?::vector score
                    FROM embeddings
                    ORDER BY
                        score
                    LIMIT ?
                     ) as closest_5 JOIN profiles p
                        ON closest_5.user_id = p.id
                    ORDER BY closest_5.score;
            """;

    private static final String SQL_CLOSEST_BY_PROFILE = """
                SELECT p.id, p.first_name, p.last_name, p.full_name, 
                    p.profile_pic_url as profile_image_url, 
                    p.linkedin_public_identifier, p.linkedin_profile_url, 
                    p.headline, p.country_full_name, p.country_code, p.state, 
                    p.city, closest_5.description
                FROM (
                    SELECT user_id, description, embeddings.clean_linkedin_profile_embedding <-> ?::vector score
                    FROM embeddings
                    ORDER BY
                        score
                    LIMIT ?
                     ) as closest_5 JOIN profiles p
                        ON closest_5.user_id = p.id
                    ORDER BY closest_5.score;
            """;

    private static final String SQL_LINKEDIN_EMBEDDING_BY_LINK = """
                SELECT e.clean_linkedin_profile_embedding
                FROM profiles p JOIN embeddings e
                    ON p.id = e.user_id
                WHERE p.linkedin_profile_url = ?
            """;

    private DataSource dataSource;
    private ClosestProfileRowMapper rowMapper;

    public List<ClosestProfileResponse> getClosestProfilesByDescription(String descriptionEmbeddings, Integer limit) {
        List<ClosestProfileResponse> result = new ArrayList<>();
        try(Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_CLOSEST_BY_DESCRIPTION);
            preparedStatement.setString(1,  descriptionEmbeddings);
            preparedStatement.setInt(2, limit);
            ResultSet rs = preparedStatement.executeQuery();
            int index = 1;
            while (rs.next()) {
                result.add(rowMapper.mapRow(rs, index++));
            }
            return result;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<ClosestProfileResponse> getClosestProfilesByProfile(String profileEmbeddings, Integer limit) {
        List<ClosestProfileResponse> result = new ArrayList<>();
        try(Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_CLOSEST_BY_PROFILE);
            preparedStatement.setString(1,  profileEmbeddings);
            preparedStatement.setInt(2, limit);
            ResultSet rs = preparedStatement.executeQuery();
            int index = 1;
            while (rs.next()) {
                result.add(rowMapper.mapRow(rs, index++));
            }
            return result;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public String getSqlLinkedinEmbeddingByLink(String linkedInProfileUrl) {
        linkedInProfileUrl = cleanLinkedInUri(linkedInProfileUrl);
        try(Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_LINKEDIN_EMBEDDING_BY_LINK);
            preparedStatement.setString(1,  linkedInProfileUrl);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return rs.getString(1);
            }
            throw new RuntimeException("No profile found!");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private String cleanLinkedInUri(String linkedInUri) {
        if (StringUtils.isBlank(linkedInUri)) return linkedInUri;
        linkedInUri = linkedInUri.replace("http://", "").replace("https://", "");
        if (linkedInUri.endsWith("/")) linkedInUri = linkedInUri.substring(0, linkedInUri.length() - 1);
        return linkedInUri;
    }

}

DROP TABLE IF EXISTS account;
create table account
(
    id                   serial      not null
        constraint account_id
            primary key,
    wallet_private_key   VARCHAR(64),
    wallet_address       VARCHAR(32),
    linkedIn_profile_uri TEXT        not null,
    title                VARCHAR(32) not null,
    activity_domain               VARCHAR(64) not null,
    first_name               VARCHAR(64) not null,
    last_name               VARCHAR(64) not null,
    profile_image               text not null
);

INSERT INTO account (linkedIn_profile_uri, title, activity_domain, first_name, last_name, profile_image)
    VALUES ('www.linkedin.com/in/tudorel-alexandru-blidea', 'Recruiter', 'IT/Engineering/Programming', 'Tudorel-Alexandru', 'Blidea',
            'https://lh3.googleusercontent.com/a/ACg8ocLIqdtcd9JwKGAS_NRueuqOpAShKLvTMRGufwvcSTTryyHoeKX5=s576-c-no');

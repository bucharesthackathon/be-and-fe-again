CREATE EXTENSION IF NOT EXISTS vector;

DROP TABLE IF EXISTS embeddings;
DROP TABLE IF EXISTS profiles;

create table profiles
(
    id                         serial       not null
        constraint id_pk
            primary key,
    first_name                 VARCHAR(64)  not null,
    last_name                  VARCHAR(64)  not null,
    full_name                  VARCHAR(128) not null,
    profile_pic_url            TEXT,
    linkedin_public_identifier VARCHAR(128) not null,
    linkedin_profile_url       VARCHAR(128) not null,
    headline                   TEXT,
    country_full_name          VARCHAR(32),
    country_code               VARCHAR(4),
    state                      VARCHAR(32),
    city                       VARCHAR(32)
);

create table embeddings
(
    id                               serial not null
        constraint id
            primary key,
    description                      TEXT   not null,
    description_embedding            vector(1536) not null,
    full_linkedin_profile            json   not null,
    clean_linkedin_profile           json   not null,
    clean_linkedin_profile_embedding vector(1536) not null,
    user_id                          serial
        constraint embeddings_to_user__fk
            references profiles
);

SELECT * FROM profiles;
SELECT * FROM embeddings;
